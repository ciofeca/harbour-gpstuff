// gpstuff -- alfonso martone

import QtQuick 2.0
import Sailfish.Silica 1.0

ApplicationWindow
{    
    initialPage: Qt.resolvedUrl("main.qml")
    cover:       Qt.resolvedUrl("cover.qml")
}

// ---
